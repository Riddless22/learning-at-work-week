# S3 bucket 
resource "aws_s3_bucket" "sridexbucket" {
  bucket = "sridexbucket"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}


# ECR resource
resource "aws_ecr_repository" "foo" {
  name                 = "srid-bar"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

# IAM role
resource "aws_iam_role" "test_role1" {
  name = "test_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}
# Python Operators are used to perform operations on variables and values.

def addition(a, b):
    total = a + b
    return total


def subtraction(a, b):
    total = a - b
    return total


def multiplication(a, b):
    total = a * b
    return total


def division(a, b):
    total = a / b
    return total


a = 10
b = 5

add_func = (addition(a, b))
sub_func = (subtraction(a, b))
times_func = (multiplication(a, b))
div_func = (division(a, b))

print(f"The value of {a} + {b} is {add_func}")
print(f"The value of {a} - {b} is {sub_func}")
print(f"The value of {a} * {b} is {times_func}")
print(f"The value of {a} / {b} is {div_func}")

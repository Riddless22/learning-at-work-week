from calculator import addition, subtraction, multiplication, division


def test_addition():
    x = addition(1, 1)
    assert x == 2


def test_subtraction():
    x = subtraction(1, 1)
    assert x == 0


def test_multiplcation():
    x = multiplication(1, 1)
    assert x == 1


def test_division():
    x = division(1, 1)
    assert x == 1
